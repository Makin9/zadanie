<?php

include 'controller/controller.php';

class LocationsController extends Controller{

	private $view;

	public function __construct(){
		$this->view = $this->loadView('locations');
	}

	// &action=index
	public function index(){
		$this->view->form();
	}

	// &action=read
	public function read(){
		$this->view->read();
	}

	// &action=create
	public function create(){
		$this->view->create();
	}

	// &action=update
	public function update(){
		$this->view->updateData();
	}

	// &action=delete
	public function delete(){
		$this->view->deleteData();
	}
}