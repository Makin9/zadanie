<?php
abstract class Controller{

    // Przekierowanie URL
    public function redirect($url){
    	header("Location: " . $url);
		exit;
    }

    public function index(){
        echo "Pusta strona";
    }

    // @param $name Nazwa pliku bez rozszerzenia
    // @param $path Ścieżka do pliku. Domyślnie folder view
    // Wczytywanie obiektu widoku z pliku.
    public function loadView($name, $path='view/'){
    	$path = $path.$name.'.php'; // ścieżka do pliku
    	$name = $name.'View'; // nazwa obiektu który powstanie
        try{
            // jeśli plik istnieje, wczytaj go i utwórz obiekt
    		if(is_file($path)){
    			require $path;
    			$obj = new $name();
    		}else{
    			throw new Exception("Nie można otworzyć" . $name . " : " . $path);
    		}
    	}
    	catch(Exception $e){
    		echo $e->getMessage();
    		exit;
    	}
    	return $obj;
    }

    // @param $name Nazwa pliku bez rozszerzenia
    // @param $path Ścieżka do pliku. Domyślnie folder model
    // Wczytywanie obiektu modelu z pliku.
    public function loadModel($name, $path='model/'){
        $path = $path.$name.'.php';
        $name = $name.'Model';
    	try{
    		if(is_file($path)){
    			require $path;
    			$obj = new $name();
    		}else{
    			throw new Exception("Nie można otworzyć" . $name . " : " . $path);
    		}
    	}
    	catch(Exception $e){
    		echo $e->getMessage();
    		exit;
    	}
    	return $obj;
    }
}
?>