<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: text/html; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type");
?>

<html>
<head>
<title>Skasuj dane</title>
<?include_once('headTemplate.html.php')?>
</head>
<body>
	<div class="container">
	<?include_once('naviTemplate.html.php')?>
	<?=$this->get('deleteItems')?>
	</div>
</body>
</html>