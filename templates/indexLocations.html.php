<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: text/html; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type");
?>
<html>
<head>
<title>Dodaj</title>
<?include_once('headTemplate.html.php')?>
</head>
<body>
	<div class="container">
	<?include_once('naviTemplate.html.php')?>
	<form class="form-inline mt-3" action="?api=locations&action=create" method="post">
		<div class="form-group">
	    Nazwa: <input type="text" name="name" required class="form-control mx-sm-3 mb-2" />
	    Szerokość: <input type="number" step="0.000001" name="lat" required class="form-control mx-sm-3 mb-2" />
	    Długość: <input type="number" step="0.000001" name="lon" required class="form-control mx-sm-3 mb-2" />
	    <input class="btn btn-primary mb-2" type="submit" value="Dodaj" />
		</div>
	</form>
</div>
</body>
</html>