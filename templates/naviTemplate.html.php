<nav class="navbar navbar-expand-lg navbar-light bg-light rounded">
	<div class="collapse navbar-collapse justify-content-md-center" id="navbarsExample10">
		<ul class="navbar-nav">
			<li class="nav-item">
				<a class="nav-link" href="?api=locations&action=read">Wyświetl</a>
            </li>
            <li class="nav-item">
				<a class="nav-link" href="?api=locations&action=index">Dodaj</a>
            </li>
            <li class="nav-item">
				<a class="nav-link" href="?api=locations&action=update">Aktualizuj</a>
            </li>
            <li class="nav-item">
				<a class="nav-link" href="?api=locations&action=delete">Usuń</a>
            </li>
		</ul>
	</div>
</nav>