<?php

include 'view/view.php';

class LocationsView extends View{

	private $model;

	public function __construct(){
		$this->model = $this->loadModel('locations');
	}
	
	public function read(){
		// ustaw zmienną locations i przypisz jej wartość z danymi w formie JSON
		$this->set('locations', $this->model->getDataJSON());
		// Wyświetla plik templates/readLocations.html.php
		$this->render('readLocations');
	}

	public function form(){
		$this->render('indexLocations');
	}

	public function create(){
		$this->model->insertItems();
	}

	public function updateData(){
		$this->set('updateItems', $this->model->getDataToUpdate());
		$this->model->updateItems();
		$this->render('updateLocations');
	}

	public function deleteData(){
		$this->set('deleteItems', $this->model->getDataToDelete());
		$this->model->deleteItems();
		$this->render('deleteLocations');
	}
}