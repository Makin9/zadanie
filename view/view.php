<?php
abstract class View{
    
    // @param $name Nazwa pliku bez rozszerzenia
    // @param $path Ścieżka do pliku. Domyślnie folder model
    // Wczytywanie obiektu modelu z pliku.
    public function loadModel($name, $path='model/'){
        $path = $path.$name.'.php';
        $name = $name.'Model';
        try{
            if(is_file($path)){
                require $path;
                $obj = new $name();
            }else{
                throw new Exception("Nie można otworzyć" . $name . " : " . $path);
            }
        }
        catch(Exception $e){
            echo $e->getMessage();
            exit;
        }
        return $obj;
    }

    // @param $name Nazwa pliku bez rozszerzenia
    // @param $path Ścieżka do pliku. Domyślnie folder templates
    // Wczytywanie i wyświetlanie szablonu strony
    public function render($name, $path='templates/'){
        $path = $path.$name.'.html.php';
        try{
            if(is_file($path)){
                require $path;
            }else{
                throw new Exception("Nie można otworzyć" . $name . " : " . $path);
            }
        }
        catch(Exception $e){
            echo $e->getMessage();
            exit;
        }
    }
    
    // Ustaw zmienną
    public function set($name, $value){
        $this->$name = $value;
    }

    // Odczytaj zmienną
    public function get($name){
        return $this->$name;
    }
}
?>