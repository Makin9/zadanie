<?php

include 'model/model.php';

class LocationsModel extends Model{
	
	// funkcja zwraca dane z tabeli w formie JSON
	public function getDataJSON() {
		if(!empty($_GET['contains']))
        	return json_encode($this->select("location", "*", "name LIKE '%".$_GET['contains']."%'"));
        else
        	return json_encode($this->select("location"));
    }

    public function insertItems(){
    	if($this->insert("location", array("name" => $_POST['name'],
						"lat" => $_POST['lat'], "lon" => $_POST['lon']))){
    		echo json_encode(array("Info" => "Pomyślnie dodano nowy rekord"));
    	}else{
    		echo json_encode(array("Info" => "Podczas dodawania rekordu wystąpił błąd"));
    	}
    }

    public function getDataToUpdate(){
    	$html = '';

    	// Pobierz dane z tabeli location
    	$arrAll = $this->select("location");

    	// Jeśli brak danych wyświetl napis
    	if($arrAll["Error"] == "Brak danych."){
    		return "<b>Brak danych</b>";
    	}

    	// Tworzenie formularza ze wszystkimi rekordami pobranymi z bazy
    	foreach ($arrAll["data"] as $val){
    		$html .= '<form class="form-inline mt-3" method="POST" action="?api=locations&action=update">
    		<div class="form-group">
    					<input type="text" name="id" value="'.$val['id'].'" hidden class="form-control mx-sm-3 mb-2" />
    					<input type="text" name="name" value="'.$val['name'].'" required class="form-control mx-sm-3 mb-2" />
    					<input type="number" step="0.000001" name="lat" value="'.$val['latitude'].'" required class="form-control mx-sm-3 mb-2" />
    					<input type="number" step="0.000001" name="lon" value="'.$val['longitude'].'" required class="form-control mx-sm-3 mb-2" />
    					<input class="btn btn-success mb-2" type="submit" name="update" value="Aktualizuj" />
    				</div></form>';
    	}
    	return $html;
    }

    public function updateItems(){
    	if(isset($_POST['update'])){
    		$this->update("location", array("id" => $_POST['id'], "name" => $_POST['name'],
						"lat" => $_POST['lat'], "lon" => $_POST['lon']));
    		header("Location: ?api=locations&action=update");
    	}
    }

    // funkcja pobiera i wyświetla dane bez możliwości edycji
    public function getDataToDelete(){
    	$html = '';
    	$arrAll = $this->select("location");
		
		if($arrAll["Error"] == "Brak danych."){
    		return "<b>Brak danych</b>";
    	}

    	foreach ($arrAll["data"] as $val){
    		$html .= '<form class="form-inline mt-3" method="POST" action="?api=locations&action=delete"><div class="form-group">
    					<input class="form-control mx-sm-3 mb-2" type="text" name="id" value="'.$val['id'].'" hidden />
    					<input readonly class="form-control mx-sm-3 mb-2" type="text" name="name" value="'.$val['name'].'" required />
    					<input readonly class="form-control mx-sm-3 mb-2" type="text" name="lat" value="'.$val['latitude'].'" required />
    					<input readonly class="form-control mx-sm-3 mb-2" type="text" name="lon" value="'.$val['longitude'].'" required />
    					<input class="btn btn-danger mb-2" type="submit" name="delete" value="Skasuj" /></div>
    				</form>';
    	}
    	return $html;
    }

    public function deleteItems(){
    	if(isset($_POST['delete'])){
    		$this->delete("location", $_POST['id']);
    		header("Location: ?api=locations&action=delete");
    	}
    }
}