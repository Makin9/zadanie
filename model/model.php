<?php
// model/model.php

abstract class Model{

    protected $pdo;

    // Konstruktor klasy
    public function __construct(){

        // Tworzenie obiektu PDO do łączenia z bazą danych na podstawie danych z config/db.php
        try{
            require 'config/db.php';
            $this->pdo = new PDO('mysql:host='.$db_host.';dbname='.$db_name, $db_usr, $db_pass);
        }
        catch(DBException $e){
            echo "Problem z połączeniem " . $e->getMessage();
        }
    }

    // @param $name Nazwa pliku bez rozszerzenia
    // @param $path Ścieżka do pliku. Domyślnie folder model
    // Wczytywanie obiektu modelu z pliku.
    public function loadModel($name, $path='model/'){
        $path = $path.$name.'.php';
        $name = $name.'Model';
        try{
            if(is_file($path)){
                require $path;
                $obj = new $name();
            }else{
                throw new Exception("Nie można otworzyć" . $name . " : " . $path);
            }
        }
        catch(Exception $e){
            echo $e->getMessage();
            exit;
        }
        return $obj;
    }

    // @param $table Nazwa tabeli
    // @param $items nazwa pól pobieranych z tabeli, domyślnie wszystkie
    // Pobieranie informacji z bazy danych
    public function select($table, $items='*', $where=NULL){
        $query = "SELECT $items FROM `$table`";
        
        if($where!=NULL){
            $query = $query." WHERE ".$where;
        }

        $stmt = $this->pdo->prepare($query);    
        $stmt->execute();
        if($stmt->rowCount() > 0){
            date_default_timezone_set('Europe/Warsaw');
            $date = date('d.m.Y H:i:s', time());
            
            // Pobieranie danych z bazy w formie tabeli asocjacyjnej
            $count = $stmt->rowCount();
            $stmt = $stmt->fetchAll(PDO::FETCH_ASSOC);
            $stmt = array("amount" => $count,
                            "table" => $table,
                            "generatedAt" => $date,
                            "data" => $stmt );
            return $stmt;
        }else{
            return array("Error" => "Brak danych.");
        }
    }

    // @param $table Nazwa tabeli
    // @param $arrData tablica z danymi
    // Dodawanie danych o lokalizacji do tabeli
    public function insert($table, $arrData){

        // sprawdzenie czy tablica z danymi nie jest pusta, jeśli tak to przerwij wykonywanie
        foreach ($arrData as $val) {
            if(empty($val)){
                return false;
            }
        }
        $query = "INSERT INTO $table SET name=:name, latitude=:latitude, longitude=:longitude";
         
        $stmt = $this->pdo->prepare($query);
        
        // Usuwanie tagów i znaczników aby wysłać do bazy czysty tekst
        $name=htmlspecialchars(strip_tags($arrData["name"]));
        $latitude=htmlspecialchars(strip_tags($arrData["lat"]));
        $longitude=htmlspecialchars(strip_tags($arrData["lon"]));
         
        // Przypisywanie wartości
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":latitude", $latitude);
        $stmt->bindParam(":longitude", $longitude);
            
        // wykonanie opercaji aktualizacji
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

    // @param $table Nazwa tabeli
    // @param $arrData tablica z danymi
    // Aktualizacja danych o lokalizacji w tabeli
    public function update($table, $arrData){
        $query = "UPDATE $table SET name=:name, latitude=:latitude, longitude=:longitude WHERE id=:id";

        $stmt = $this->pdo->prepare($query);

        // Usuwanie tagów i znaczników aby wysłać do bazy czysty tekst
        $id=htmlspecialchars(strip_tags($arrData["id"]));
        $name=htmlspecialchars(strip_tags($arrData["name"]));
        $latitude=htmlspecialchars(strip_tags($arrData["lat"]));
        $longitude=htmlspecialchars(strip_tags($arrData["lon"]));
         
         // Przypisywanie wartości
        $stmt->bindParam(":id", $id);
        $stmt->bindParam(":name", $name);
        $stmt->bindParam(":latitude", $latitude);
        $stmt->bindParam(":longitude", $longitude);

        // wykonanie opercaji aktualizacji
        if($stmt->execute()){
            return true;
        }
 
        return false;
    }

    // @param $table Nazwa tabeli
    // @param $id identyfikator lokalizacji do kasacji
    // Kasowanie danych o lokalizacji w tabeli
    public function delete($table, $id){
        $query = "DELETE FROM $table WHERE id=$id";
        $stmt = $this->pdo->prepare($query);
        if($stmt->execute()){
            return true;
        }
        return false;
    }
}
?>